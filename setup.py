# -*- coding: utf-8 -*-
"""Application configuration for pip installation.

Author: Julian Kunzelmann
"""
from setuptools import setup, find_packages

setup(
    name='whatsterror',
    version='0.0.1',
    packages=find_packages(exclude=["tests", "*.tests", "*.tests.*",
                                    "bin", "scripts"]),
    entry_points={
        'console_scripts': ['whatsterror = whatsterror.__main__:main']
    },
    url='',
    license='BSD 3-clause',
    author='Master Blaster',
    author_email='private',
    description='Terror arrogant whatsapp user.'
)

