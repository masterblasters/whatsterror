# -*- coding: utf-8 -*-

"""Main terror.

Author: Master Blaster
"""

from __future__ import print_function

# Standard libraries.
from argparse import ArgumentParser, RawTextHelpFormatter
import time
import sys
import datetime as dt

# Own libraries.
try:
    from .whatsterror import get_whatsweb, write_msg
except ValueError as e:
    from whatsterror import get_whatsweb, write_msg
    print('Run the application with makefile, please. Error: {}'.format(e))


def main():
    # Parse arguments.
    args = parse_arguments()

    if None in [args.msg, args.nmb, args.rec, args.sleep]:
        print('Wrong arguments. For help: python -m whatsterror --help')
        exit()
    else:
        msg = args.msg
        nmb = int(args.nmb)
        if nmb < 1:
            print("Error: Number argument has to be positive.")
            sys.exit()

        rec = args.rec
        sleep = float(args.sleep)

    if args.pp is not None:
        try:
            due = dt.datetime.strptime(args.pp, '%Y-%m-%d %H:%M')
        except Exception:
            print('Error: invalid time format. format="YYYY-mm-dd HH:MM"')
            sys.exit()

        print("Message will be sent at: {}".format(args.pp))
    else:
        to_wait = 0

    print('Use your phone to connect to whatsapp web!')
    wweb = get_whatsweb()

    if args.pp is not None:
        now = dt.datetime.now()
        to_wait = (due - now).seconds
        time.sleep(to_wait)

    for i in range(nmb):
        for submsg in msg:
            write_msg(wweb, rec, submsg)
            time.sleep(sleep)

    wweb.quit()


def parse_arguments():
    '''Argument parser.

    Returns:
        args (Namespace): Parsed arguments.
    '''

    usage = \
        "python -m whatsterror [-r RECIPIENT -s SLEEP -m MESSAGE -n NUMBER]" \
        "\n\nUsage: python -m whatserror -r kay -s 60 -m 'Minute' -n 10\n" \
        "  or python -m whatsterror -r tom -s 30 -m half minute -n 5\n"

    parser = ArgumentParser(description="Revenge their arrogance.",
                            usage=usage, formatter_class=RawTextHelpFormatter)
    parser.add_argument("-r", "--recipient", dest="rec", default=None,
                        help="recipient of the message.")
    parser.add_argument("-s", "--sleep", dest="sleep", default=None,
                        help="Seconds until repeating message.")
    parser.add_argument('-m', '--message', dest='msg', default=None, nargs='+',
                        help='message to deliver.')
    parser.add_argument('-n', '--number', dest='nmb', default=None,
                        help='Number of messages until terminate.')
    parser.add_argument('-p', '--postpone', dest='pp', default=None,
                        help='Postpone till given time. '
                        'format=YYYY-MM-DD hh:mm')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    main()
    print('DONE')
