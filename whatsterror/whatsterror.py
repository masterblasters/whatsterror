# -*- coding: utf-8 -*-

"""Terror functions.

Author: Master Blaster
"""  # pragma: no cover

from __future__ import print_function  # pragma: no cover

# Standard libraries.
from selenium import webdriver  # pragma: no cover
import time  # pragma: no cover


def get_whatsweb():  # pragma: no cover
    '''Open browser with whatsweb and wait for validation.
    '''
    driver = webdriver.Firefox()
    driver.get('https://web.whatsapp.com')

    x = None
    while x is None:
        try:
            driver.find_element_by_xpath(
                "//*[text()[contains(.,'Search or start new chat')]]")
            break
        except Exception:
            time.sleep(5)

    return driver


def write_msg(web, rec, msg):  # pragma: no cover
    '''Write the message to the recipient.
    '''
    user = web.find_elements_by_xpath('//span[@title = "{}"]'.format(rec))
    if isinstance(user, list):
        user[0].click()
    else:
        user.click()

    msg_box = web.find_element_by_class_name('_2S1VP')

    msg_box.send_keys(msg)
    button = web.find_element_by_class_name('_35EW6')
    button.click()
