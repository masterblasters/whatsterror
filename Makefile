# Makefile for apps.

PROJ = $(shell basename "$$PWD")

RUN = python -m
CHECKSTYLE = flake8
DTEST = python -m doctest
UTEST = coverage run --source=./$(PROJ) -m unittest discover -s ./$(PROJ)
UTESTCOV = coverage report -m --omit=*__*__.py,*test_*.py


all: checkstyle test

# Run style checker.
checkstyle:
	$(CHECKSTYLE) $(PROJ)

# Run doctests and unittests.
test: test-doc test-unit test-coverage

# Run application.
run:
	$(RUN) $(PROJ)

# Print help.
help:
	$(RUN) $(PROJ) --help

# Clean folder.
clean: clean-pycache clean-coverage clean-bytecode

# Create documentation.
doc:
	if ! [ -f docs/conf.py ]; then make doc-init; fi;
	cd docs && sphinx-apidoc -f -o source --private ../$(PROJ)
	cd docs && make html

# Install in current python environment.
install:
	pip install . --upgrade

# Uninstall in current python environment.
uninstall:
	pip uninstall . $(PROJ)


# Print usage text.
usage:
	echo make ^<command^>
	echo Main commands:
	echo    checkstyle:	Check the code style conventions.
	echo    test:		Run docstring tests, unit tests and show code coverage.
	echo    install:	Install app as console app in the current environment.
	echo    run:		Execute application locally.
	echo    clean:		Remove non-essential files.
	echo    doc:		Create html documentation in docs folder.



##########################################################

# Run doctests.
test-doc:
	find $(PROJ) -name "*.py" -not -regex ".*[__,´tests]*"  -exec $(DTEST) {} +

# Run unittests.
test-unit:
	$(UTEST)

# Check coverage of unittest run.
test-coverage:
	$(UTESTCOV)

# Remove pycache folder.
clean-pycache:
	find . -type d -name '*__pycache__' -exec rm -rf {} +

# Remove coverage info.
clean-coverage:
	find . -type f -name '*.coverage' -exec rm -f {} +

# Remove generated python bytecode.
clean-bytecode:
	find . -type f -name '*.pyc' -exec rm -f {} +
	find . -type f -name '*.pyd' -exec rm -f {} +
	find . -type f -name '*.pyo' -exec rm -f {} +

# Initialize sphinx documentation.
doc-init:
	if ! [ -d docs ]; then mkdir docs; fi;
	cd docs && sphinx-quickstart
	echo >> docs/conf.py
	echo import os>> docs/conf.py
	echo import sys>> docs/conf.py
	echo >> docs/conf.py
	echo "sys.path.insert(0, os.path.abspath('..'))">> docs/conf.py
	echo "sys.path.insert(1, os.path.abspath('../$(PROJ)'))">> docs/conf.py
	echo >> docs/conf.py
	echo "def skip(app, what, name, obj, skip, options):">> docs/conf.py
	echo "    if name == '__init__' or name == '__main__':">> docs/conf.py
	echo "        return False">> docs/conf.py
	echo "    return skip">> docs/conf.py
	echo >> docs/conf.py
	echo "def setup(app):">> docs/conf.py
	echo "    try:">> docs/conf.py
	echo "        app.connect('autodoc-skip-member', skip)">> docs/conf.py
	echo "    except Exception:">> docs/conf.py
	echo "        print('No autodoc included.')">> docs/conf.py
